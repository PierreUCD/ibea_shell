function y = costFunction(xUnit, m)
    % cost function for IBEA
    % m: number of objectives
    % xUnit: 11 by 1 vector. Contains values between 0 and 1 output by IBEA
    
    if m~=2
        error('Function only works with two objectives');
    end

    y = zeros(1, 2); 

    xReal = unitToReal(xUnit);

    % write Ansys parameter file
    fid = fopen('param_file.txt','w');
    fprintf(fid,'%3.8E\n',xReal);
    fclose(fid);

    % open Ansys with input file
    system('ansys180 -p aa_t_a -np 2 -s read -dir . -b nolist < ./SSSS_Comp_centre.inp > out/output.out 2>&1');
    
    % read results computed from Ansys
    fid = fopen('stress.txt','r');
    y(1) = fscanf(fid,'%f');
    fclose(fid);

    fid=fopen('mass.txt','r');
    y(2) = fscanf(fid,'%f');
    fclose(fid);

end



