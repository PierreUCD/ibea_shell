function [indices] = pareto_front_with_duplicates(Y)
    % Y = A n by m matrix of objectives, where m is the number of objectives
    % and n is the number of points
    %
    % copes with duplicates
    % assumes minimisation
    [n,m] = size(Y);
    S = zeros(n,1);

    for i=1:n
        % get number of points that dominate Y
        S(i) = sum((sum(Y<=repmat(Y(i,:),n,1),2) == m) & (sum(Y<repmat(Y(i,:),n,1),2) > 0));
    end
    indices = find(S==0);
    
end