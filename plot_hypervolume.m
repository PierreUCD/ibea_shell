function plot_hypervolume(filename, scale)
% plot hypervolume vs nb of generations
% plot final hypervolume on pareto front
% filename: .mat file name with optimisation results (eg 'res/ibea_20_200.mat')
% scale = 1 if scale objectives, else use aboslute reference point

%close all;

load(filename);

pop_size = 20;
nbGen = length(samples_objectives)/pop_size - 1;
pt_space = 1; 
data_pt = nbGen/pt_space;

So_i = samples_objectives;

if scale == 1
    upb = max(So_i);
    lwb = min(So_i);
    So_i = rescale_objectives(So_i,upb,lwb);
    refpoint = ones(1,2);
else
    max_stress = 2e11;
    max_mass = 40;
    refpoint = [max_stress max_mass];
end

v_i = zeros(data_pt,1);
vp_i = zeros(data_pt,1);

for i=1:data_pt
    i
    fflush(stdout);
    bound = pt_space*i;
    [v_i(i) vp_i(i)] = hypervolume2d(So_i(1:bound,:), refpoint);
end

x = 1:pt_space:nbGen;

figure;
plot(x,v_i,'r*-');

xlabel('Number of generations');
ylabel('Hypervolume');

hypervolume2d(So_i, refpoint, 1);
title('Final hypervolume plot');
xlabel('Max XY stress');
ylabel('Total mass');

%saveas(1,'res/convergence_20_200.pdf');
%saveas(2,'res/hypervolume_20_200.pdf');

%--------------------------------------------------------------------------
function Xo_scaled = rescale_objectives(Xo,upb,lwb)
% rescale each objectives to the interval [0,1]

n = size(Xo,1);
Xo_scaled = (Xo-repmat(lwb,n,1))./repmat(upb-lwb,n,1);