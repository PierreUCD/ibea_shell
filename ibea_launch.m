% IBEA launch script

clear all;

% optimisation parameters
pop_size = 20;
generations = 800;
cost_function = 'costFunction';
l = 11;
num_obj = 2;
p_mut = 0.01;
kappa = 0.05;

% initialise populatation or not
init = 1;

if init

    old_filename = 'res/ibea_20_60.mat';     % don't forget to change at every run: to automatise later
    load(old_filename, 'X');
    load(old_filename, 'Xo');
    load(old_filename, 'samples');
    load(old_filename, 'samples_objectives');
    
    old_X = X;
    old_Xo = Xo;
    old_samples = samples;
    old_samples_o = samples_objectives;
    
else
    old_X = [];
    old_Xo = [];
    old_samples = [];
    old_samples_o = [];
    
end    

% run IBEA
[Archive, Archive_objectives, X, Xo, samples, samples_objectives] = IBEA(pop_size, generations, cost_function, l, num_obj, p_mut, kappa, init, old_X, old_Xo, old_samples, old_samples_o);

% save and plot results
dataFileName = strcat('res/ibea_', int2str(pop_size) ,'_', int2str(length(samples)),'.mat');

save(dataFileName);

plot_ibea(dataFileName);
plot_hypervolume(dataFileName, 1);
