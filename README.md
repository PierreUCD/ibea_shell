# README #

### What is this repository for? ###

* optimisation of a simply supported composite plate with a central load
* objectives: minimise the mass and the maximum xy stress
* optimiser: Indicator Based Evolutioary Algorithm
* finite element package: Ansys
* design parameters: ply thicknesses, ply angles and material properties

### How do I get set up? ###

* add Ansys to the PATH: copy the following line in the ~/.bashrc file
> export PATH="/usr/ansys_inc/v180/ansys/bin:$PATH"
* clone repository
> $ git clone https://PierreUCD@bitbucket.org/PierreUCD/ibea_shell.git
* edit optimisation parameters
> $ vim ibea_launch.m
* run with Octave/Matlab
> $ octave ibea_launch.m
