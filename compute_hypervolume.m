function [ vol ] = compute_hypervolume( P, R, k )
%function [ vol ] = compute_hypervolume( P, R, k )
%
% Exact hypervolume calculation based on algorithm from Bader and Zitzler
% Note, reference points need to be dominated by all set members or set
% collapses and result is an empty matrix!
%
% TIK-Report 286, 2008
%
% P = matrix. non-dominated set of objective vectors, n by m (n number of
% vectors, m number of objectives
% R = reference point, 1 by m, must be dominated by *all* members of P
% k -- as a recursive algorithm, is used by subsequent calls, but should
% not be passed as initial argument
%
% Implementation (c) Jonathan Fieldsend    

[n,m]=size(P);

if exist('k','var')==0
    k=n; 
end
F.a=P;          %Pareto points
F.v=zeros(n,1); %volume
[n1,m1]=size(R);
if (m1~=m)
    error('Objective dimension of population and reference point do not match');
end

 vol=do_slicing(F,R,k,m,1,inf*ones(1,m));
 
 %vol2 = sanity_check(P,R)
end


function [F_prime] = do_slicing(F,R,k,i,V,z)
% current fitness assignment F
% reference set R
% fitness parameter k
% recusion level i
% partition volume V
% scan positions z
F_prime = [];
[nr,m]=size(R);
if (i==m)
    UP=F.a;
    UR=R;
else
    % filter out relevant solutions  and reference points
    %if (isempty(F)==0)
        [nf,m]=size(F.a);
        % index of all elements of F which dominate z
        %Iup = (sum((F.a(:,i+1:m)<=repmat(z(i+1:m),nf,1)),2)==(m-i)); 
        I=zeros(nf,1);
        for j=i+1:m
            I=I+(F.a(:,j)<=z(j));
        end
        Iup=(I==(m-i));
        UP = F.a(Iup,:);
    %else
    %    UP=[];
    %end
    % index of all elements of R which dominate z
    %I = (sum((R(:,i+1:m)>=repmat(z(i+1:m),nr,1)),2)==(m-i));
    I=zeros(nr,1);
    for j=i+1:m
        I=I+(R(:,j)>=z(j));
    end
    I=(I==(m-i));
    UR = R(I,:);
end
if ((i==0) && (isempty(UR)==0))
    %end of recursion
    [nup,m]=size(UP);
    alpha = (k-(1:(nup-1)))./(nf-(1:(nup-1)));
    alpha = prod(alpha);
    %index of all members of F dominating z
    %Iup = (sum((F.a<=repmat(z,nf,1)),2)==m);
    %by def Iup is the index of all dominating members of F when here
    F_prime=F;
    F_prime.v(Iup) = F.v(Iup) +  (alpha/nup)*V;
elseif (i>0)
    %recursion continues
    F_prime = F;
    U = [UP; UR];
    [tmp,I]=sort(U(:,i));
    U=U(I,:);
    %scan current dimension in ascending order
    while (isempty(U)==0)
       u_star = min(U(:,i));
       I=U(:,i)>u_star;
       U_prime = U(I,:);
       if (isempty(U_prime)==0)
          V_prime = V*(min(U_prime(:,i))-u_star);
          z_temp=z;
          z_temp(i)=u_star;
          F_prime = do_slicing(F_prime,R,k,i-1,V_prime,z_temp);
       end
       U=U_prime;
    end
end



end

function v = sanity_check(P,R)

z=100000;
[n,m]=size(P);
[n2,m]=size(R);
mp=rand(z,m);
domed=zeros(z,1);
nullify_dom=zeros(z,1);
C=[P;R];
mx=max(C);
mn=min(C);
%convert random sample to minimum bounding hypercube
mp=(mp.*repmat(mx-mn,z,1))+repmat(mn,z,1); 
for i=1:z
    for j=1:n
        if (sum(P(j,:)<=mp(i,:))==m)
            tag=0;
            for k=1:n2
                if (sum(mp(i,:)<=R(k,:))==m)
                    tag=tag+1;
                end
            end
            if (tag >= 1)
                domed(i)=1;
                break;
            end
        end
    end
end
%plot3(mp(:,1),mp(:,2),mp(:,3),'.')
%proportion of space dominated
prop_domed = (sum(domed)-sum(nullify_dom))/z;
%volume of space dominated
v = mx-mn;
v = prod(v); %get total volume of hyperrectangle
v = v * prop_domed; %reweight by proportion covered

end

