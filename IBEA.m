function [Archive, Archive_objectives, X, Xo, samples, samples_objectives] = IBEA(pop_size, generations, cost_function, l, num_obj, p_mut, kappa, init, old_X, old_Xo, old_samples, old_samples_o)

% Implements the adaptive IBEA_epsilon+ algorithm described in 2004 PPSN paper by
% Zitzler and Kunzli
%
% inputs:
% pop_size = number of mmebers in search population
% generations = number of iterations of algorithm
% cost_function = string containing the name of the objective
%   function to optimise, must take as arguments the decision vector
%   followed by the number of objectives, and return an array (1 by
%   D) of the D objectives evaluated
% l = number of decision parameters
% num_obj = number of objectives
% p_mut = probability of mutation, in paper, 0.01 for continous problems
% kappa = discount factor for indicator, 0.05 in paper
% init: if init = 1, the initial population is from a previous run with a 'samples' history
% otherwise, the initial population is allocated randomly
% old_A = archive output of pervious run -- set as empty set [] if you do
%         not wish to restart from previous run
% old_Ao = archive objectives output of pervious run -- set as empty set [] 
%          if you do not wish to restart from previous run
% old_samples = samples output of pervious run -- set as empty set [] if you do
%         not wish to restart from previous run
% old_samples_o = samples objectives output of pervious run -- set as empty set [] 
%          if you do not wish to restart from previous run
%
% Uses simulated binary crossover (20 genes) and assumes legal
% bounds on decision parameters as [ 0, 1 ]
% returns:
%
% Archive = matrix of archive decision vectors
% Archive_objectives = matrix of archive member evaluations
% samples = history of algorithm state in terms of its locations evaluated
% samples_objectives = corresponding objectives
%
% (c) 2013 Jonathan Fieldsend, University of Exeter


% TO CHECK IF IT@S OK FOR UCD PROBLEM

%if ~exist('p_mut','var')
%    p_mut = 1;
%else
%    p_mut = ceil(p_mut*l);
%end
%% p_mut now holds the number of elements of a vector to flip each time
%if ~exist('kappa','var')
%    kappa = 0.05;
%end

if ~exist('p_mut','var')
    p_mut = 0.01;
end
if ~exist('kappa','var')
    kappa = 0.05;
end
if ~exist('old_X','var')
    old_X=[];
end
if ~exist('old_Xo','var')
    old_X=[];
end
if ~exist('old_samples','var')
    old_samples = [];
end
if ~exist('old_samples_o','var')
    old_samples_o = [];
end

SBX_n=20;

% INITIALISATION
% generates an initial population of size pop_size
[samples, samples_objectives, sample_index, X, Xo] = initialise(pop_size, init, generations, l, num_obj, cost_function, old_X, old_Xo, old_samples, old_samples_o);

mating_pool = rand(pop_size,l);
offspring = rand(pop_size,l);
off_o = rand(pop_size,num_obj); 

for kk=1:generations % loop for generations

    % counters
    count_env = 0;
    count_cross = 0;
    count_mut = 0;

    % FITNESS ASSIGNMENT
    Xo_scaled = rescale_objectives(Xo);
    [fitness,c] = fitness_assignment(Xo_scaled,kappa);

    % ENVIRONMENTAL SELECTION
    while size(X,1)>pop_size
        count_env ++;
    
        [~,j] = min(fitness);
        X(j,:) =[];
        ty = Xo_scaled(j,:);
        Xo(j,:) = [];
        Xo_scaled(j,:)=[];
        fitness(j) = [];
        fitness = update_fitness(fitness,Xo_scaled,kappa,ty,c);
    end
     
    % MATING SELECTION
    for j=1:pop_size;
        I=randperm(pop_size);
        %binary tournament selection on fitness value with replacement
        if fitness(I(1))<fitness(I(2))
            mating_pool(j,:)=X(I(1),:);
        else
            mating_pool(j,:)=X(I(2),:);
        end            
    end
    
    % Variation, SBX-20 operator is used for recombination and a polynomial distribution for mutation
    %CROSSOVER
    offspring = mating_pool;
    for j=1:2:pop_size-1;
        for k=1:l
            if rand()<0.5 %0.5 probability of crossing over variable
                count_cross ++;
                valid=0;
                while (valid==0)
                    u=rand();
                    if (u<0.5)
                        beta=(2*u)^(1/(SBX_n+1));
                    else
                        beta=(0.5/(1-u))^(1/(SBX_n+1));
                    end
                    mean_p=0.5*(mating_pool(j,k)+mating_pool(j+1,k));
                    %c1= 0.5*((1+beta)*Population(j,k)+(1-beta)*Population(j+1,k));
                    %c2= 0.5*((1-beta)*Population(j,k)+(1+beta)*Population(j+1,k));
                    c1=mean_p-0.5*beta*abs(mating_pool(j,k)-mating_pool(j+1,k));
                    c2=mean_p+0.5*beta*abs(mating_pool(j,k)-mating_pool(j+1,k));   
                    if (c1>=0.0 && c1<=1.0) && (c2>=0.0 && c2<=1.0)
                        valid=1;
                    end
                end
                offspring(j,k)=c1;
                offspring(j+1,k)=c2;
            end
        end
    end
    
    %MUTATION
    for j=1:pop_size-1;
        for k=1:l
            if rand()<p_mut
                count_mut ++;
                y=2;
                while ((y<0) || (y>1.0))
                    r=rand();
                    if r<0.5
                        delta=(2*r)^(1/(SBX_n+1))-1;
                    else
                        delta=1-(2*(1-r))^(1/(SBX_n+1));
                    end
                    y=offspring(j,k)+delta; %range of variables is 1, so just add delta
                end
                offspring(j,k)=y;
            end
        end
    end
    
    % add offspring to population 
    for i=1:pop_size
        off_o(i,:) = feval(cost_function,offspring(i,:),num_obj);
        samples(sample_index,:) = offspring(i,:);
        samples_objectives(sample_index,:) = off_o(i,:);
        sample_index = sample_index+1;
    end
    if rem(kk,1)==0
        fprintf('Iteration %d, Evaluation %d, nb_env_selections %d, nb_crossovers %d, nb_mutations %d\n',kk,kk*pop_size+pop_size, count_env, count_cross, count_mut);
        fflush(stdout);
        save('res/temp_ibea.mat');
    end
    Xo = [Xo; off_o];
    X = [X; offspring];
    
end

% TERMINATION
I = pareto_front_with_duplicates(Xo);
Archive = X(I,:);
Archive_objectives = Xo(I,:);

%----------------------------------------------
function [samples, samples_objectives, sample_index, X, Xo] = initialise(pop_size, init, generations, l, num_obj, cost_function, old_X, old_Xo, old_samples, old_samples_o)
% initialise the population from a previous run history (init=1) or with random
% individuals otherwise

if init % if initial population is created from previous run with a 'sample' history
    
    if size(old_X,1) ~= 2*pop_size
        error('old population size does not match the population size now being used -- the old population should be twice the pop_size argument');
    end
        
    X = old_X;
    Xo = old_Xo;
    samples = [old_samples; zeros(generations*pop_size+size(X,1),l)];
    samples_objectives = [old_samples_o; zeros(generations*pop_size+size(Xo,1),num_obj)];
    sample_index = size(old_samples,1);
    
    samples(sample_index+1:sample_index+size(X,1),:) = X;
    samples_objectives(sample_index+1:sample_index+size(Xo,1),:) = Xo;
    sample_index = sample_index+size(Xo,1)+1;   
    
else % if no initial population
        
    samples = zeros((generations+1)*pop_size,l);
    samples_objectives = zeros((generations+1)*pop_size,num_obj);
    % declare archive and associated objective evaluations as empty
    % Create random indiviual (Uniform) bits and evaluate
    X = rand(pop_size,l);
    Xo = zeros(pop_size,num_obj);
    
    for i=1:pop_size
        Xo(i,:) = feval(cost_function,X(i,:),num_obj);
    end
    
    samples(1:pop_size,:) = X;
    samples_objectives(1:pop_size,:) = Xo;
    sample_index = pop_size+1;
    
end

%----------------------------------------------
function Xo_scaled = rescale_objectives(Xo)

n = size(Xo,1);
upb = max(Xo);
lwb = min(Xo);

Xo_scaled = (Xo-repmat(lwb,n,1))./repmat(upb-lwb,n,1);
%----------------------------------------------
function [fitness,c] = fitness_assignment(Xo,kappa)

[n,m] = size(Xo);
fitness = zeros(n,1);
indicator = zeros(n,n);

for i=1:n
    for j=1:n
        if i~=j
            indicator(i,j) = max(Xo(i,:)-Xo(j,:)); % get shift value
            %fitness(i) = fitness(i) -exp(-indicator/kappa); 
        end
    end
end

c =max(max(indicator));

for j=1:n
    fitness(j) = sum(-exp(-indicator(:,j)/(c*kappa)));
end


%----------------------------------------------
function fitness = update_fitness(fitness,Xo,kappa,old_val,c)

n = size(Xo,1);
for i=1:n
    indicator = max(old_val-Xo(i,:)); % get shift value
    fitness(i) = fitness(i) + exp(-indicator/(c*kappa)); 
end

%----------------------------------------------
function [indices] = pareto_front_with_duplicates(Y)
% Y = A n by m matrix of objectives, where m is the number of objectives
% and n is the number of points
%
% copes with duplicates
% assumes minimisation

[n,m] = size(Y);
S = zeros(n,1);


for i=1:n
    % get number of points that dominate Y
    S(i) = sum((sum(Y<=repmat(Y(i,:),n,1),2) == m) & (sum(Y<repmat(Y(i,:),n,1),2) > 0));
end
indices = find(S==0);

%     
% %----------------------------------------------
% function x= dominates(u,v)
% 
% dom=u<v;
% wdom=u<=v;
% if (sum(wdom)==length(u) && sum(dom)>0)
%     x=1;
% else
%     x=0;
% end
% %----------------------------------------------
% function indices= set_dominates(U,v)
% 
% %U = set of objective vectors, n by num_objectives
% %v = single objective vector, 1 by num_objectives
% 
% Iwd=zeros(size(U,1),1);
% Id=zeros(size(U,1),1);
% for i=1:length(v)
%     Iwd=Iwd+(U(:,i)<=v(i));
%     Id=Id+(U(:,i)<v(i));
% end
% indices = find((Iwd==length(v))+(Id>0)==2);
% %----------------------------------------------
% function indices= dominates_set(v,U)
% 
% %U = set of objective vectors, n by num_objectives
% %v = single objective vector, 1 by num_objectives
% 
% Iwd=zeros(size(U,1),1);
% Id=zeros(size(U,1),1);
% for i=1:length(v)
%     Iwd=Iwd+(U(:,i)>=v(i));
%     Id=Id+(U(:,i)>v(i));
% end
% 
% indices = find((Iwd==length(v))+(Id>0)==2);
% %----------------------------------------------
% function [A,Ao,fitness]=reduce_Pareto_set(A,Ao,fitness,max_archive_size)
% 
% %calculate point distances once
% raw_d=zeros(size(A,1),size(A,1));
% for i=1:size(A,1)
%     d=dist2(Ao(i,:),Ao);
%     raw_d(i,:)=d;
% end
% 
% while (size(A,1)>max_archive_size)
%     D=zeros(size(A,1),size(A,1));
%     for i=1:size(A,1);
%         D(i,:)=sort(raw_d(i,:));
%     end
%     k=2; %how many k looking out to, k=1 it itself, so incrementing from 2
%     II=1:size(A,1); %will use as index to lowest distance points
%     while length(II)>1 && k<=size(A,1)
%         m=min(D(II,k));
%         I=find(D(II,k)==m); %indices of all points with min distance
%         II=II(I); %update index with subset
%         k=k+1;
%     end
%     if k>size(A,1) %gotten to end
%         II=II(1); % points must be duplicates so take out first
%     end
%     A(II,:)=[];
%     Ao(II,:)=[];
%     fitness(II)=[];
%     D(II,:)=[];
%     %remove relavent distances from matrix
%     raw_d(II,:)=[];
%     raw_d(:,II)=[];
% end
% 
