function plot_ibea(res_filename, plot_filename)
    % plot the pareto front from the result file 'res_filename' (.mat file)
    % save the results under plot_filename

    %close all;
    load(res_filename);

    % sort Archive and Archive_objectives by ascending mass
    [~,I] = sort(Archive_objectives(:,2));
    sorted_archive_o = Archive_objectives(I,:);
    sorted_archive = Archive(I,:);

    % configuration with a median masssss
    middle = ceil(size(sorted_archive,1)/2);

    %% mass - percent convertion
    %mass_none = 0.09391844617;
    %sorted_archive_o(:,2) = (sorted_archive_o(:,2)*100/mass_none)-100;
    %samples_objectives(:,2) = (samples_objectives(:,2)*100/mass_none)-100;
    %
    % create color for scatter option
    len=size(samples_objectives,1);
    C=zeros(len,1);

    for i=1:len
        C(i,1)=i;
    end

    ft_size = 12;

    % Pareto plot
    figure;
    orient landscape;
    hold on;

    scatter(samples_objectives(:,1),samples_objectives(:,2),5,C,'.');
    plot(sorted_archive_o(:,1),sorted_archive_o(:,2),'-ko','MarkerSize',10,'MarkerFaceColor','r','MarkerEdgeColor','k','LineWidth',2);
    plot(sorted_archive_o(middle,1),sorted_archive_o(middle,2),'-ko','MarkerSize',10,'MarkerFaceColor','g','MarkerEdgeColor','k','LineWidth',2);


    h_legend = legend('Evaluated configurations','Non-dominated solutions', 'printed config'); 
    set(h_legend,'Fontsize',ft_size);
    xlabel('max Sxy (Pa)','Fontsize',ft_size, 'Interpreter','latex'); 
    ylabel('mass (kg)','Fontsize',ft_size); 
    title('Simply supported plate','Fontsize',ft_size);    

    colormap jet;
    h = colorbar();%'YTickLabel',{'280','560','840','1120','1400'});
    %set(h,'YTick',1:size(samples_objectives,1)/20-1:size(samples_objectives,1));
    ylabel(h, 'Number of evaluations','Fontsize',ft_size);

    %axis([min(samples_objectives(:,1)),max(samples_objectives(:,1)),min(samples_objectives(:,2)),max(samples_objectives(:,2))]);
    %saveas(1, plot_filename);
    
    % write best config in human readable format
    write_config(sorted_archive, sorted_archive_o, middle);
    

