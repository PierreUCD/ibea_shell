function write_config(Archive, Archive_objectives, id)
    % write Archives number id to text file
    
    % convert from unit to real value
    y = unitToReal(Archive(id, :));

    % ansys format
    fid = fopen('best_param_ansys.txt','w');
    fprintf(fid,'%3.8E\n', y);
    fclose(fid);

    % human format
    fid = fopen('best_param_human.txt','w');
    fprintf(fid,'%20s %2.2e\n','Max Sxy (Pa): ', Archive_objectives(id, 1));
    fprintf(fid,'%20s %2.2f\n\n','Total mass (kg): ', Archive_objectives(id, 2));

    fprintf(fid,'%20s %2.2f\n','h1 (mm): ', y(1)*1000);
    fprintf(fid,'%20s %2.2f\n','h2 (mm): ', y(2)*1000);
    fprintf(fid,'%20s %2.2f\n','theta1 (deg): ', y(3));
    fprintf(fid,'%20s %2.2f\n','theta2 (deg): ', y(4));
    fprintf(fid,'%20s %2.2e\n','EX (Pa): ', y(5));
    fprintf(fid,'%20s %2.2e\n','EY (Pa): ', y(6));
    fprintf(fid,'%20s %2.2e\n','EZ (Pa): ', y(7));
    fprintf(fid,'%20s %2.2f\n','v: ', y(8));
    fprintf(fid,'%20s %2.2e\n','GXY (Pa): ', y(9));
    fprintf(fid,'%20s %2.2e\n','GYZ (Pa): ', y(10));
    fprintf(fid,'%20s %2.2e\n','GXZ (Pa): ', y(11));
    fclose(fid);