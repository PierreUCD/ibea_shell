clear all;
close all;

load ibea_20_20.mat;
so_new = samples_objectives;

fid=fopen('so_new.txt','w');
fprintf(fid,'%f %f\n',so_new);
fclose(fid);

load ibea_20_20_fix.mat;
so = samples_objectives;
so = so(1:420, :);

fid=fopen('so.txt','w');
fprintf(fid,'%f %f\n',so);
fclose(fid);



%%%%%

%
%fid=fopen('so_new.txt','r');
%so = fscanf(fid,'%f %f\n');
%fclose(fid);

%fid=fopen('so.txt','r');
%so = fscanf(fid,'%f %f\n');
%fclose(fid);


%filename = 'so_new.txt';
%delimiterIn = ' ';
%so = importdata(filename,delimiterIn);

filename = 'so.txt';
delimiterIn = ' ';
so = importdata(filename,delimiterIn);


len = size(so,1);
C = zeros(len,1);

for i=1:len
    C(i,1)=i;
end


rows(so(:,1))
rows(so(:,2))
rows(C)

figure(1);
scatter(so(:,1),so(:,2),10,C,'.');
