% solve NaN problem

close all;
clear all;
load ibea_20_22.mat;

yo = 1:11;

figure();
hold on;


noBug_Ao = Archive;
noBug_Ao([5 10 17 28], :) = [];

for i = 1:length(noBug_Ao)
    plot(yo, noBug_Ao(i, :), 'ro-','LineWidth',1);
end


plot(yo, Archive(5, :), 'kx-','LineWidth',3);
plot(yo, Archive(10, :), 'kx-','LineWidth',3);
plot(yo, Archive(17, :), 'kx-','LineWidth',3);
plot(yo, Archive(28, :), 'kx-','LineWidth',3);

ylabel('scaled parameter value');
xlabel('parameters');

set(gca, 'xtick', [1 2 3 4 5 6 7 8 9 10 11]);
set(gca, 'xticklabel', {'h1', 'h2', 't1', 't2', 'EX','EY','EZ','v','GXY','GYZ','GXZ'});
get(gca, 'xticklabelmode'); 

%x = 0:200; 
%plot (x) 
%set (gca, 'xtick', [0 100 200]) 
%set (gca, 'xticklabel', {'zero', 'one hundred', 'two   hundred'}) 


% convert to real values
for i = 1:length(Archive)
    Archive(i, :) = unitToReal(Archive(i, :));
end

bug_Ao(1, :) = Archive(5, :);
bug_Ao(2, :) = Archive(10, :);
bug_Ao(3, :) = Archive(17, :);
bug_Ao(4, :) = Archive(28, :);

noBug_Ao = Archive;
noBug_Ao([5 10 17 28], :) = [];

x = ones(size(bug_Ao(:, 1)), 1);
xx = ones(size(noBug_Ao(:, 1)), 1);
%
%figure();
%hold on;
%plot(x, bug_Ao(:, 1), 'kx', 'MarkerSize', 10, 'LineWidth',2);
%plot(xx, noBug_Ao(:, 1), 'ro', 'MarkerSize', 10, 'LineWidth',2);

% for Ansys
fid = fopen('bug_param.txt','w');
fprintf(fid,'%3.8E\n', Archive(5, :));
fclose(fid);

% transpose
Archive = transpose(Archive);

fileID = fopen('bug_NaN.txt','w');
fprintf(fileID,'%15s %15s %15s %15s %15s %15s %15s %15s %15s %15s %15s\n',...
'h1 (mm)','h2 (mm)', 'theta1 (deg)','theta2 (deg)','EX (Pa)','EY (Pa)','EZ (Pa)','v','GXY (Pa)','GYZ (Pa)','GXZ (Pa)');
fprintf(fileID,'%15.2e %15.2e %15.2e %15.2e %15.2e %15.2e %15.2e %15.2e %15.2e %15.2e %15.2e\n', Archive);
fclose(fileID);
