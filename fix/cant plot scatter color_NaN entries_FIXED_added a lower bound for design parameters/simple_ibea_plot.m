% simple IBEA pareto plot

load ibea_20_20.mat;

figure(1);
hold on;

%% create color for scatter plot
len = size(samples_objectives, 1);
C = zeros(len, 1);

%x = linspace(0,3*pi,200);
%y = cos(x) + rand(1,200);
sz = 25;
c = linspace(1,10,length(samples_objectives));
%scatter(x,y,sz,c,'filled')
c = transpose(c)

for i = 1:len
    C(i, 1) = i;
end

scatter(samples_objectives(:,1),samples_objectives(:,2), 40, c, '.')

%plot(samples_objectives(:, 1), samples_objectives(:, 2), 'b.');
plot(Archive_objectives(:, 1), Archive_objectives(:, 2), 'ro');

xlabel('max Sxy (Pa)'); 
ylabel('mass (kg)');

