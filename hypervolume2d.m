function [v vp A] = hypervolume2d(A, refpoint, plot_flag)

% function v = hypervolume2d(A,refpoint)
%
% function returns hypervolume in 2d space defined as the area of the
% the retangle whose corners are the origin and the 2d vector argument
% 'refpoint' which is dominated by the nondominated set 'A'
%
% A = nondominated set 'n by 2'
% refpoint = reference point '1 by 2'
% plot_flag = flag to indicate the result should also be plotted,
%             if it exists as an argument, plotting will occur
%
% (c) Jonathan Fieldsend, University of Exeter


if exist('plot_flag','var')==0
    plot_flag=0;
end
if length(refpoint)~=2
    error('reference point should be a 2d vector');
end

[n,m] = size(A);
if m~=2
    error('archive should be an n by 2 matrix');
end

I =zeros(n,1);
% extract non-dominated set
for i=1:n; 
    % I(i) = (sum(sum(repmat(A(i,:),n,1)>=A,2)==m) < 2); % assumes no duplicates
    
    % copes with duplicates (to check with Jonathan)
    
    
    I(i) = sum((sum(A<=repmat(A(i,:),n,1),2) == m) & (sum(A<repmat(A(i,:),n,1),2) > 0))==0;  
end

A = A(I==1,:);

[n,m] = size(A);
[~,I]= sort(A(:,1)); % sort first dimension of A, smallest to largest 
A = A(I,:); % archive now sorted

% systematically go through archive, calculating areas dominated by archive

v = 0;
if plot_flag
    figure;
    hold on;
    axis([0 refpoint(1) 0 refpoint(2)]);
end
backpoint = refpoint;
for i = 1:n
    if sum( A(i,:) <= refpoint) == 2 %check point dominates refpoint
        if (i>1)
            if sum(A(i,:)==A(i-1,:))~=1 % check no duplicate points
                v = v + prod(backpoint - A(i,:));
                if plot_flag
                    plot(A(i,1),A(i,2),'.');
                    x = [A(i,1) backpoint(1) backpoint(1) A(i,1) A(i,1)];
                    y = [A(i,2) A(i,2) backpoint(2) backpoint(2) A(i,2)];
                    plot(x,y,'r');
                end
                backpoint(2) = A(i,2);
            end
        end
    end
end

vp = v/ prod(refpoint);