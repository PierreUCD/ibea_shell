function y = realToUnit(x)
% convert design vector with real values x from Ansys to a unit vector readable by IBEA 

    n = length(x);
    y = zeros(n, 1);
    
    % h1
    maxh1 = 0.03;
    minh1 = 0.001;
    y(1) = (x(1)-minh1)/(maxh1-minh1);
    
    % h2
    maxh2 = 0.03;
    minh2 = 0.001;
    y(2) = (x(2)-minh2)*(maxh2-minh2);
    
    % theta1
    maxTheta1 = 180;
    y(3) = x(3)*maxTheta1;
    
    % theta2
    maxTheta2 = 180;
    y(4) = x(4)*maxTheta2;
    
    % EX
    maxEX = 200e9;
    minEX = 1e9;
    y(5) = (x(5)-minEX)*(maxEX-minEX);
    
    % EY
    maxEY = 200e9;
    minEY = 1e9;
    y(6) = (x(6)-minEY)*(maxEY-minEY);
    
    % EZ
    maxEZ = 200e9;
    minEZ = 1e9;
    y(7) = (x(7)-minEZ)*(maxEZ-minEZ);
    
    % v
    maxV = 0.5;
    minV = 0.1;
    y(8) = (x(8)-minV)*(maxV-minV);
    
    % GXY
    maxGX = 80e9;
    minGX = 1e9;
    y(9) = (x(9)-minGX)*(maxGX-minGX);
    
    % GYZ
    maxGY = 80e9;
    minGY = 1e9;
    y(10) = (x(10)-minGY)*(maxGY-minGY);
    
    % GXZ
    maxGZ = 80e9;
    minGZ = 1e9;
    y(11) = (x(11)-minGZ)*(maxGZ-minGZ);
    
end
