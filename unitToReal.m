function y = unitToReal(x)
% convert unit design vector x from IBEA to a format readable by Ansys 

    n = length(x);
    y = zeros(n, 1);
    
    % h1
    maxh1 = 0.03;
    minh1 = 0.001;
    y(1) = minh1+x(1)*(maxh1-minh1);
    
    % h2
    maxh2 = 0.03;
    minh2 = 0.001;
    y(2) = minh2+x(2)*(maxh2-minh2);
    
    % theta1
    maxTheta1 = 180;
    y(3) = x(3)*maxTheta1;
    
    % theta2
    maxTheta2 = 180;
    y(4) = x(4)*maxTheta2;
    
    % EX
    maxEX = 200e9;
    minEX = 1e9;
    y(5) = minEX+x(5)*(maxEX-minEX);
    
    % EY
    maxEY = 200e9;
    minEY = 1e9;
    y(6) = minEY+x(6)*(maxEY-minEY);
    
    % EZ
    maxEZ = 200e9;
    minEZ = 1e9;
    y(7) = minEZ+x(7)*(maxEZ-minEZ);
    
    % v
    maxV = 0.5;
    minV = 0.1;
    y(8) = minV+x(8)*(maxV-minV);
    
    % GXY
    maxGX = 80e9;
    minGX = 1e9;
    y(9) = minGX+x(9)*(maxGX-minGX);
    
    % GYZ
    maxGY = 80e9;
    minGY = 1e9;
    y(10) = minGY+x(10)*(maxGY-minGY);
    
    % GXZ
    maxGZ = 80e9;
    minGZ = 1e9;
    y(11) = minGZ+x(11)*(maxGZ-minGZ);
    
end
